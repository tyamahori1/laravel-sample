<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', static function () {
    logger('test', ['message' => 'sample']);

    return [
        'time' => 'test2',
        'pram' => Request::input('param', 'now'),
        'ip'   => Request::ip(),
    ];
});
